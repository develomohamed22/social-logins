<?php
use Illuminate\Support\Facades\Route;

Route::get('login-facebook/{type}','Socials\SocialLogin\socialapp\FacebookController@login_app')->name('login-facebook');
Route::get('facebook-login','Socials\SocialLogin\socialapp\FacebookController@socialApp')->name('social-login');

Route::get('login-google/{type}','Socials\SocialLogin\socialapp\GoogleController@login_app')->name('login-google');
Route::get('google-login','Socials\SocialLogin\socialapp\GoogleController@socialApp')->name('social-login');

Route::get('login-linkedin/{type}','Socials\SocialLogin\socialapp\LinkedInController@login_app')->name('login-linkedin');
Route::get('linkedin-login','socialapp\LinkedInController@socialApp')->name('social-login');
